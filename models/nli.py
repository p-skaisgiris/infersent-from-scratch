import torch
import torch.nn as nn


class InferSent(nn.Module):
    def __init__(
        self,
        sentence_enc: nn.Module,
        num_classes: int,
        hid_size: int = 512,
        debug: bool = False,
    ):
        super(InferSent, self).__init__()
        self.debug = debug
        self.sentence_enc = sentence_enc
        in_size = self.sentence_enc.get_output_size()
        # We multiply the input by 4 because we will perform concatenation of 4
        # types of sentences
        self.fc1 = nn.Linear(in_features=in_size * 4, out_features=hid_size)
        # Feed the hidden state to produce the required number of classes
        self.fc2 = nn.Linear(in_features=hid_size, out_features=num_classes)

    def forward(self, premise, premise_lengths, hypothesis, hypothesis_lengths):
        premise_enc = self.sentence_enc(premise, premise_lengths)
        hypothesis_enc = self.sentence_enc(hypothesis, hypothesis_lengths)
        if self.debug:
            print("premise_enc shape:", premise_enc.shape)
            print("hypothesis_enc shape:", hypothesis_enc.shape)

        out = torch.cat(
            (
                premise_enc,
                hypothesis_enc,
                torch.abs(premise_enc - hypothesis_enc),
                premise_enc * hypothesis_enc,
            ),
            axis=1,
        )
        if self.debug:
            print("concatenated shape:", out.shape)

        out = self.fc1(out)
        if self.debug:
            print("concat after fc1 shape:", out.shape)

        out = self.fc2(out)
        if self.debug:
            print("concat after fc2 shape:", out.shape)

        return out
