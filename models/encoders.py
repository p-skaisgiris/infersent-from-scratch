import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


class BaseModel(nn.Module):
    def __init__(
        self,
        embed_dim: int,
        emb_vectors: torch.Tensor,
        hid_size: int = 2048,
        debug: bool = False,
    ):
        super(BaseModel, self).__init__()
        self.embed_dim = embed_dim
        self.debug = debug

        # Initialize the embedding layer with the slightly modified glove vectors
        self.embedding = nn.Embedding.from_pretrained(
            embeddings=emb_vectors, freeze=True
        )

    def get_output_size(self) -> int:
        return self.embed_dim


class AverageGloVe(BaseModel):
    def forward(self, text_as_idx, text_lengths):
        out = self.embedding(text_as_idx)
        if self.debug:
            print("after embedding:", out.shape)

        out = torch.mean(out, dim=1)
        if self.debug:
            print("after mean", out.shape)

        return out

    def __str__(self):
        return "Average"


class UniLSTM(BaseModel):
    def __init__(
        self,
        embed_dim: int,
        emb_vectors: torch.Tensor,
        hid_size: int = 2048,
        debug: bool = False,
    ):
        super(UniLSTM, self).__init__(
            embed_dim=embed_dim, emb_vectors=emb_vectors, debug=debug
        )
        self.hid_size = hid_size
        self.lstm = nn.LSTM(
            input_size=self.embed_dim, hidden_size=hid_size, num_layers=1
        )

    def get_output_size(self) -> int:
        return self.hid_size

    def forward(self, text_as_idx, text_lengths):
        x = self.embedding(text_as_idx)
        if self.debug:
            print("after embedding", x.shape)

        # Pack the input to represent the padded input in a more efficient way
        packed_embedded = pack_padded_sequence(
            x, text_lengths.cpu(), batch_first=True, enforce_sorted=False
        )
        if self.debug:
            print("after packing:", packed_embedded.data.shape)

        _, (out, _) = self.lstm(packed_embedded)
        if self.debug:
            print("after LSTM:", out.shape)

        # (1, batch_size, EMBED_DIM) to (batch_size, EMBED_DIM)
        out = out.squeeze()
        return out

    def __str__(self):
        return "UniLSTM"


class BiLSTM(BaseModel):
    def __init__(
        self,
        embed_dim: int,
        emb_vectors: torch.Tensor,
        hid_size: int = 2048,
        debug: bool = False,
    ):
        super(BiLSTM, self).__init__(
            embed_dim=embed_dim, emb_vectors=emb_vectors, debug=debug
        )
        self.hid_size = hid_size
        self.lstm = nn.LSTM(
            input_size=self.embed_dim,
            hidden_size=hid_size,
            num_layers=1,
            bidirectional=True,
        )

    def get_output_size(self) -> int:
        return self.hid_size * 2

    def forward(self, text_as_idx, text_lengths):
        x = self.embedding(text_as_idx)
        if self.debug:
            print("after embedding:", x.shape)

        packed_embedded = pack_padded_sequence(
            x, text_lengths.cpu(), batch_first=True, enforce_sorted=False
        )
        if self.debug:
            print("after packing:", packed_embedded.data.shape)

        # Take the final hidden state for each element in the sequence
        _, (out, _) = self.lstm(packed_embedded)
        if self.debug:
            print("after LSTM:", out.shape)

        # Concatenating the forward and backward LSTM states
        out = torch.cat((out[0], out[1]), axis=1)
        if self.debug:
            print("after concatenation:", out.shape)

        return out

    def __str__(self):
        return "BiLSTM"


class BiLSTMMaxPool(BaseModel):
    def __init__(
        self,
        embed_dim: int,
        emb_vectors: torch.Tensor,
        hid_size: int = 2048,
        debug: bool = False,
    ):
        super(BiLSTMMaxPool, self).__init__(
            embed_dim=embed_dim, emb_vectors=emb_vectors, debug=debug
        )
        self.hid_size = hid_size
        self.lstm = nn.LSTM(
            input_size=self.embed_dim,
            hidden_size=hid_size,
            num_layers=1,
            bidirectional=True,
        )

    def get_output_size(self) -> int:
        return self.hid_size * 2

    def forward(self, text_as_idx, text_lengths):
        x = self.embedding(text_as_idx)
        if self.debug:
            print("after embedding:", x.shape)

        packed_embedded = pack_padded_sequence(
            x, text_lengths.cpu(), batch_first=True, enforce_sorted=False
        )
        if self.debug:
            print("packed_embedded:", packed_embedded.data.shape)

        # Take the hidden LSTM outputs after each token
        out, _ = self.lstm(packed_embedded)
        if self.debug:
            print("after LSTM:", out.data.shape)

        # TODO: concatenate forward and backward?

        out = pad_packed_sequence(out)[0]
        if self.debug:
            print("after pad:", out.shape)

        # Apply max pooling over the one dimension
        out = out.max(axis=0).values
        if self.debug:
            print("after max:", out.shape)

        return out

    def __str__(self):
        return "BiLSTMMaxPool"
