import torch
import utils.project_config as config
from datasets import load_dataset
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import DataLoader
from torchtext.data import get_tokenizer
from torchtext.data.functional import to_map_style_dataset


def load_snli():
    return load_dataset("stanfordnlp/snli")


def load_snli_splits():
    # Load data from HuggingFace
    if config.DEBUG:
        train_data, val_data, test_data = load_dataset(
            "stanfordnlp/snli",
            split=[
                f"train[:{10*config.BATCH_SIZE}]",
                f"validation[:{10*config.BATCH_SIZE}]",
                f"test[:{10*config.BATCH_SIZE}]",
            ],
        )
    else:
        train_data, val_data, test_data = load_dataset(
            "stanfordnlp/snli",
            split=["train", "validation", "test"],
        )

    # From https://huggingface.co/datasets/stanfordnlp/snli#data-fields
    # documentation about the SNLI dataset:
    # "Dataset instances which don't have any gold label are marked with -1
    # label. Make sure you filter them before starting the training using
    # `datasets.Dataset.filter`."
    train_data = train_data.filter(lambda row: row["label"] != -1)
    val_data = val_data.filter(lambda row: row["label"] != -1)
    test_data = test_data.filter(lambda row: row["label"] != -1)

    return train_data, val_data, test_data


def collate_batch(batch, vocab, tokenizer):
    label_list, premise_list, hypothesis_list = [], [], []

    for b in batch:
        label_list.append(b["label"])

        # Convert text to indices

        # lowercase, tokenize words and make a list out of the word indices
        # in the vocabulary to represent a sentence
        processed_premise = torch.tensor(
            vocab(tokenizer(b["premise"])), dtype=torch.int64
        )
        premise_list.append(processed_premise)

        processed_hypothesis = torch.tensor(
            vocab(tokenizer(b["hypothesis"])), dtype=torch.int64
        )
        hypothesis_list.append(processed_hypothesis)

    # Save original length
    premise_lengths = torch.tensor([sent.size(0) for sent in premise_list])
    hypothesis_lengths = torch.tensor([sent.size(0) for sent in hypothesis_list])

    # Pad the variable lengths sentences to all be the length of the longest sequence
    padded_premises = pad_sequence(premise_list, batch_first=True, padding_value=0)
    padded_hypotheses = pad_sequence(hypothesis_list, batch_first=True, padding_value=0)

    label_list = torch.tensor(label_list, dtype=torch.int64)

    return (
        label_list,
        padded_premises,
        premise_lengths,
        padded_hypotheses,
        hypothesis_lengths,
    )


def get_snli_dataloaders(vocab) -> tuple[DataLoader, DataLoader, DataLoader]:
    train_data, val_data, test_data = load_snli_splits()

    # Download and load tokenizer
    tokenizer = get_tokenizer("basic_english")

    # This is needed to create the dataloaders successfully
    train_data = to_map_style_dataset(train_data)
    val_data = to_map_style_dataset(val_data)
    test_data = to_map_style_dataset(test_data)

    # Define a custom collate function which takes in a vocab
    collate_fn = lambda batch: collate_batch(  # noqa
        batch, vocab=vocab, tokenizer=tokenizer
    )

    data_loader_kwargs = {
        "batch_size": config.BATCH_SIZE,
        "collate_fn": collate_fn,
        "num_workers": config.NUM_WORKERS,
        "pin_memory": True,
    }

    train_loader = DataLoader(train_data, **data_loader_kwargs)
    val_loader = DataLoader(val_data, **data_loader_kwargs)
    test_loader = DataLoader(test_data, **data_loader_kwargs)

    return train_loader, val_loader, test_loader
