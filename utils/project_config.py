import torch

DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# In the InferSent paper they used this batch size
BATCH_SIZE: int = 64
# Speed up the dataloader with multiple workers.
# NOTE: does not seem to work when using cuda
NUM_WORKERS: int = 0
# Whether to output shapes of most tensors during training and validation
DEBUG: bool = False
EMBED_DIM = 300
NUM_CLASSES = 3
