import os
import re
from collections import OrderedDict

import torch
from models.encoders import AverageGloVe, BiLSTM, BiLSTMMaxPool, UniLSTM
from models.nli import InferSent

STR_TO_MODEL_CLS = {
    "unilstm": UniLSTM,
    "bilstm": BiLSTM,
    "bilstm_max": BiLSTMMaxPool,
    "average": AverageGloVe,
}


def fix_state_dict(state_dict, load_embeddings: bool = True):
    # Reference: https://discuss.pytorch.org/t/solved-keyerror-unexpected-key-module-encoder-embedding-weight-in-state-dict/1686/3  # noqa
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        # Don't load the weights for embedding, keep the above-loaded vectors
        if not load_embeddings:
            if k.startswith("model.sentence_enc"):
                continue

        name = k[6:]  # remove `model.`
        new_state_dict[name] = v
    return new_state_dict


def get_max_dir(directories: list):
    # Reference: https://stackoverflow.com/questions/43074685/find-file-in-directory-with-the-highest-number-in-the-filename  # noqa

    def extract_number(f):
        s = re.findall("\d+$", f)  # noqa
        return (int(s[0]) if s else -1, f)

    return max(directories, key=extract_number)


def get_state_dict_from_ckpt(model_name: str, load_embeddings: bool, path_to_logs: str):
    # FIXME: model_dirs[0] and os.listdir(ckpt_dir)[0]

    # Get current model directory
    model_dirs = [
        dir_name
        for dir_name in os.listdir(path_to_logs)
        if dir_name.startswith(model_name)
    ]
    ckpt_dir = os.path.join(path_to_logs, model_dirs[0])
    # Get the max version directory
    max_version = get_max_dir(os.listdir(ckpt_dir))
    ckpt_dir = os.path.join(ckpt_dir, max_version, "checkpoints")
    # Get the checkpoint file and compute final path
    ckpt_dir = os.path.join(ckpt_dir, os.listdir(ckpt_dir)[0])

    print(f"Loading: {ckpt_dir}")
    if torch.cuda.is_available():
        checkpoint = torch.load(ckpt_dir)
    else:
        checkpoint = torch.load(ckpt_dir, map_location=torch.device("cpu"))

    # Potentially, do not load the embedding weights
    state_dict = fix_state_dict(
        checkpoint["state_dict"], load_embeddings=load_embeddings
    )
    return state_dict


def create_model(
    model_name: str,
    emb_vectors: torch.Tensor,
    embed_dim: int = 300,
    enc_hid_size: int = 2048,
    nli_hid_size: int = 512,
    num_classes: int = 3,
    debug: bool = False,
):
    model_cls = STR_TO_MODEL_CLS[model_name]
    sentence_enc = model_cls(
        embed_dim=embed_dim,
        emb_vectors=emb_vectors,
        hid_size=enc_hid_size,
        debug=debug,
    )
    model = InferSent(
        sentence_enc=sentence_enc,
        hid_size=nli_hid_size,
        num_classes=num_classes,
        debug=debug,
    )
    return model


def load_model(
    model_name: str,
    emb_vectors: torch.Tensor,
    path_to_logs: str,
    embed_dim: int = 300,
    enc_hid_size: int = 2048,
    nli_hid_size: int = 512,
    num_classes: int = 3,
    debug: bool = False,
    load_embeddings: bool = True,
    strict: bool = False,
):
    model = create_model(
        model_name=model_name,
        emb_vectors=emb_vectors,
        embed_dim=embed_dim,
        enc_hid_size=enc_hid_size,
        nli_hid_size=nli_hid_size,
        num_classes=num_classes,
        debug=debug,
    )

    if model_name.startswith("bilstm"):
        enc_hid_size = enc_hid_size * 2

    model_name = f"{str(model.sentence_enc)}_{enc_hid_size}_{nli_hid_size}"
    state_dict = get_state_dict_from_ckpt(
        model_name, load_embeddings=load_embeddings, path_to_logs=path_to_logs
    )

    model.load_state_dict(state_dict, strict=strict)
    return model
