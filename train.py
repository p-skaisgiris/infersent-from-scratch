import argparse

import lightning as L

# Important to import like this and NOT
# from utils.project_config ...
# as otherwise we cannot change the values of the config globally!
import utils.project_config as config
from lightning.pytorch import seed_everything
from lightning.pytorch.callbacks import EarlyStopping, LearningRateMonitor
from lightning.pytorch.loggers import TensorBoardLogger
from trainer import LitInferSent
from utils.dataset import get_snli_dataloaders
from utils.model_loading import create_model
from utils.vocabulary import prepare_vocab_vectors


def get_args():
    parser = argparse.ArgumentParser(
        description="Training script for InferSent models."
    )
    parser.add_argument(
        "--models",
        nargs="+",
        help=(
            "Specify the model types to train. Can be one or more. For example,"
            " --models unilstm OR --models unilstm bilstm bilstm_max. Average is a"
            " baseline model that is not trained."
        ),
        required=True,
        choices=["average", "unilstm", "bilstm", "bilstm_max"],
    )
    parser.add_argument(
        "--enc-hid-size",
        help=(
            "Specify the hidden size of the encoder. Note that the same hidden size"
            " will be applied to all specified models. The default is 2048, as in the"
            " InferSent paper."
        ),
        default=2048,
        type=int,
        required=False,
    )
    parser.add_argument(
        "--nli-hid-size",
        help=(
            "Specify the hidden size of the NLI model. The default is 512, as in the"
            " InferSent paper."
        ),
        default=512,
        type=int,
        required=False,
    )
    parser.add_argument(
        "-c",
        "--ckpt-path",
        help="Path to save logs and checkpoints of models. Default is 'tb_logs'.",
        default="tb_logs",
        required=False,
    )
    parser.add_argument(
        "--device",
        help=(
            "Specify the device to train on. If not specified, it will check for the"
            " device automatically and run the training using it."
        ),
        default=None,
        choices=["cpu", "cuda"],
        required=False,
    )
    parser.add_argument(
        "--seed",
        help="Specify the seed for reproducibility of results. Defaults to 1234.",
        default=1234,
        type=int,
        required=False,
    )
    parser.add_argument(
        "--debug",
        help=(
            "Whether to run the training in DEBUG mode. This will print shapes of"
            " intermediary tensors and reduce the training and validation dataset sizes"
            " to 10 batches. Default is False."
        ),
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def main(args):
    seed_everything(args.seed, workers=True)

    print("Preparing vocab and vectors...")
    vocab, vectors = prepare_vocab_vectors()

    print("Getting data...")
    train_loader, val_loader, test_loader = get_snli_dataloaders(vocab)

    print("Creating models...")

    for enc_model in args.models:
        print(f"\n============ {str(enc_model)} =============\n")
        infersent = create_model(
            model_name=enc_model,
            emb_vectors=vectors,
            embed_dim=config.EMBED_DIM,
            enc_hid_size=args.enc_hid_size,
            nli_hid_size=args.nli_hid_size,
            num_classes=config.NUM_CLASSES,
            debug=args.debug,
        )
        model = LitInferSent(model=infersent)

        # "training is stopped when the learning rate goes under the threshold of 10−5"
        early_stop = EarlyStopping(
            monitor="lr-SGD", stopping_threshold=10**-5, mode="min"
        )
        # checkpoint = ModelCheckpoint(monitor="val_loss")
        lr_monitor = LearningRateMonitor(logging_interval="step")

        enc = infersent.sentence_enc
        experiment_name = f"{str(enc)}_{enc.get_output_size()}_{args.nli_hid_size}"
        training_conf = {
            # Conservation of energy
            "max_epochs": 15,
            "max_time": "00:03:50:00",
            "logger": TensorBoardLogger(args.ckpt_path, name=experiment_name),
            "callbacks": [early_stop, lr_monitor],
            "deterministic": True,
        }
        if enc_model == "average":
            # Since we only care about the encoder and nothing is optimized there
            training_conf["max_epochs"] = 1

        # Train model using GPU if possible
        lit_trainer = (
            L.Trainer(**training_conf)
            if str(config.DEVICE) == "cpu"
            else L.Trainer(devices=1, accelerator="gpu", **training_conf)
        )

        print("Fitting model...")
        lit_trainer.fit(
            model=model, train_dataloaders=train_loader, val_dataloaders=val_loader
        )
        lit_trainer.test(model=model, dataloaders=test_loader)
        # TODO: write results to file


if __name__ == "__main__":
    args = get_args()
    # Re-define the global variables based on the arguments
    config.DEBUG = args.debug
    if args.device is not None:
        config.DEVICE = args.device
    main(args)
