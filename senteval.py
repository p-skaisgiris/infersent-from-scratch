import argparse
import datetime
import logging
import math
import os
import pickle
import sys

import torch
from torch.nn.utils.rnn import pad_sequence
from torchtext.data import get_tokenizer
from utils.model_loading import load_model
from utils.vocabulary import prepare_vocab_vectors

# Set PATHs
# path to senteval
PATH_TO_SENTEVAL = "../SentEval"
# path to the NLP datasets
PATH_TO_DATA = "../SentEval/data"
# path to glove embeddings
PATH_TO_VEC = ".vector_cache/glove.840B.300d.txt"
PATH_TO_TBLOGS = "tb_logs/"

sys.path.insert(0, PATH_TO_SENTEVAL)
import senteval  # noqa

# Set params for SentEval

# Default parameters from the SentEval README.md
params_senteval = {"task_path": PATH_TO_DATA, "usepytorch": True, "kfold": 10}
# "The tool uses Adam to fit a logistic regression classifier, with batch size 64"
params_senteval["classifier"] = {
    "nhid": 0,
    "optim": "adam",
    "batch_size": 64,
    "tenacity": 5,
    "epoch_size": 4,
}

# Set up logger
logging.basicConfig(format="%(asctime)s : %(message)s", level=logging.DEBUG)


def get_args():
    parser = argparse.ArgumentParser(
        description="Script to evaluate embeddings using SentEval."
    )
    parser.add_argument(
        "--models",
        nargs="+",
        help=(
            "Specify the model types to train. Can be one or more. For example,"
            " --models unilstm OR --models unilstm bilstm bilstm_max. Average is a"
            " baseline model that is not trained."
        ),
        required=True,
        choices=["average", "unilstm", "bilstm", "bilstm_max"],
    )

    return parser.parse_args()


def collate_batch(batch, vocab, tokenizer):
    sentence_list = []

    for b in batch:
        # Make a list out of the word indices in the vocabulary to represent a sentence
        # Here, the sentences are already tokenized
        processed_sentence = torch.tensor(vocab(b), dtype=torch.int64)
        sentence_list.append(processed_sentence)

    # Save original length
    sent_lengths = torch.tensor([sent.size(0) for sent in sentence_list])

    # Pad the variable lengths sentences to all be the length of the longest sequence
    padded_sentences = pad_sequence(sentence_list, batch_first=True, padding_value=0)
    return padded_sentences, sent_lengths


def batcher(params, batch):
    logging.debug(f"Batch: {params.current_batch}/{params.total_batches}")
    params.current_batch += 1

    # If sentence is empty, change this sentence to a single unknown token
    batch = [list(sent) if len(sent) > 0 else ["<unk>"] for sent in batch]
    sentences, sent_lengths = collate_batch(batch, params.vocab, params.tokenizer)
    embeddings = params.encoder(sentences, sent_lengths)
    return embeddings.detach().numpy()


def yield_sentence_tokens(data):
    for sentence in data:
        yield sentence


def prepare(params, samples, model_name: str):
    """
    In this example we are going to load Glove,
    here you will initialize your model.
    remember to add what you model needs into the params dictionary
    """
    params["total_batches"] = math.ceil(len(samples) / params["batch_size"])
    params["current_batch"] = 1

    # Align the vocab and vectors to the current dataset
    vocab, vectors = prepare_vocab_vectors(yield_sentence_tokens(samples))

    # Load the whole NLI model
    model = load_model(
        model_name=model_name,
        emb_vectors=vectors,
        load_embeddings=False,
        strict=False,
        path_to_logs=PATH_TO_TBLOGS,
    )

    # But keep only the encoder for SentEval
    params["encoder"] = model.sentence_enc
    params["vocab"] = vocab
    params["tokenizer"] = get_tokenizer("basic_english")


if __name__ == "__main__":
    args = get_args()

    transfer_tasks = [
        "MR",
        "CR",
        "SUBJ",
        "MPQA",
        "SST2",
        "TREC",
        "MRPC",
        "SICKRelatedness",
        "SICKEntailment",
        "STS14",
    ]
    for enc_model in args.models:
        mod_prepare = lambda params, samples: prepare(  # noqa
            params, samples, enc_model
        )
        se = senteval.engine.SE(params_senteval, batcher, mod_prepare)

        # senteval prints the results and returns a dictionary with the scores
        res = se.eval(transfer_tasks)
        print(res)

        if not os.path.exists("results/"):
            os.makedirs("results/")

        timestamp = round(datetime.datetime.now().timestamp())
        with open(f"results/{str(enc_model)}_{timestamp}.pickle", "wb") as f:
            pickle.dump(res, f)
