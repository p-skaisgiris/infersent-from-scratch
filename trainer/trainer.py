import lightning as L
import torch
import torch.nn.functional as F
from torcheval.metrics.functional import multiclass_accuracy


class LitInferSent(L.LightningModule):
    def __init__(self, model):
        super().__init__()
        self.model = model
        self.best_val_accuracy = torch.tensor(0)
        self.current_accuracy = torch.tensor(0)
        self.val_preds = []
        self.val_labels = []
        self.test_preds = []
        self.test_labels = []

    def training_step(self, batch, batch_idx):
        label, premise, premise_length, hypothesis, hypothesis_length = batch

        pred = self.model(premise, premise_length, hypothesis, hypothesis_length)

        loss = F.cross_entropy(pred, label)
        self.log("train_loss", loss)

        return loss

    def validation_step(self, batch, batch_idx):
        label, premise, premise_length, hypothesis, hypothesis_length = batch

        pred = self.model(premise, premise_length, hypothesis, hypothesis_length)

        loss = F.cross_entropy(pred, label)
        self.log("val_loss", loss)

        self.val_preds.extend(pred.argmax(dim=1))
        self.val_labels.extend(label)

        return loss

    def test_step(self, batch, batch_idx):
        label, premise, premise_length, hypothesis, hypothesis_length = batch

        pred = self.model(premise, premise_length, hypothesis, hypothesis_length)

        loss = F.cross_entropy(pred, label)
        self.log("test_loss", loss)

        self.test_preds.extend(pred.argmax(dim=1))
        self.test_labels.extend(label)

        return loss

    def on_validation_epoch_end(self):
        all_preds = torch.tensor(self.val_preds)
        all_labels = torch.tensor(self.val_labels)
        accuracy = multiclass_accuracy(all_preds, all_labels)

        self.current_accuracy = accuracy
        self.log("val_accuracy", accuracy)

        # TODO: Set to manual optimization! These schedulers' steps are
        # applied IN ADDITION to the automatic schedulers - it happens twice!
        sch1, sch2 = self.lr_schedulers()
        # Decay of learning rate is applied after each epoch
        sch1.step()

        # "At each epoch, we divide the learning rate by 5 if the
        # dev accuracy decreases."
        # I looked at the original InferSent code and it decreases w.r.t
        # the best dev accuracy
        if self.current_accuracy > self.best_val_accuracy:
            self.best_val_accuracy = self.current_accuracy
        else:
            sch2.step()

        # Release memory
        self.val_preds.clear()
        self.val_labels.clear()

    def on_test_epoch_end(self):
        all_preds = torch.tensor(self.test_preds)
        all_labels = torch.tensor(self.test_labels)
        accuracy = multiclass_accuracy(all_preds, all_labels)
        self.log("test_accuracy", accuracy)
        self.test_preds.clear()
        self.test_labels.clear()

    def configure_optimizers(self):
        # "we use SGD with a learning rate of 0.1"
        optimizer = torch.optim.SGD(self.parameters(), lr=0.1)
        # "...and a weight decay of 0.99"
        sch1 = torch.optim.lr_scheduler.MultiplicativeLR(
            optimizer, lr_lambda=lambda epoch: 0.99
        )
        # "At each epoch, we divide the learning rate by 5 if the dev
        # accuracy decreases."
        sch2 = torch.optim.lr_scheduler.MultiplicativeLR(
            optimizer, lr_lambda=lambda epoch: 0.2
        )
        return [optimizer], [sch1, sch2]
