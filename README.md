# infersent-from-scratch

This repository is a modular and extendable PyTorch Lightning re-implementation of [InferSent](https://aclanthology.org/D17-1070/) sentence representations by Conneau et al., 2017.

We train four types of sentence encoders - average, unidirectional LSTM, bidirectional LSTM, bidirectional LSTM with max pooling - via the natural language inference task using the SNLI dataset. This is done to create sentence representations which are general and transferable to other tasks. Refer to the paper for more information.

Original results:

| **models**   | **dim** | **dev_acc** | **test_acc** | **micro** | **macro** |
|--------------|---------|-------------|--------------|-----------|-----------|
| average      | 300     | NaN         | NaN          | NaN       | NaN       |
| unilstm-last | 2048    | 81.9        | 80.7         | 79.5      | 78.6      |
| bigru-last   | 4096    | 81.3        | 80.9         | 82.9      | 81.7      |
| bilstm-max   | 4096    | 85.0        | 84.5         | 85.2      | 83.7      |

My results:

|   **models** | **dim** | **dev_acc** | **test_acc** | **micro** | **macro** |
|-------------:|--------:|------------:|-------------:|----------:|----------:|
|      average |     300 |        60.8 |         61.4 |      82.6 |      79.1 |
| unilstm-last |    2048 |        75.4 |         75.3 |      72.9 |      70.1 |
|  bilstm-last |    4096 |        76.4 |         76.8 |      81.1 |      78.5 |
|   bilstm-max |    4096 |        83.4 |         83.4 |      85.7 |      83.0 |

[Here](https://wim.nl.tab.digital/s/QPrTSA6X92C9RoR) you can download the trained models and tensorboard logs (`tb_logs/`) as well as the training, senteval jobs and their outputs (`jobs/`).

## Overview of repository

`train.py` is the script for training encoders via NLI. Refer to usage instructions below.

`senteval.py` is the script for evaluating sentence representations using SentEval. Refer to usage instructions below.

`pipeline.ipynb` is the first sweep for the development of this repository. As such, it is not fully up-to-date with the rest of the repo. However, it offers initial analyses of the dataset and motivates some design choices such as filtering the dataset, adding padding and unknown tokens and their vectors.

`analysis.ipynb` is the notebook showing final results and using the trained models. It should be read in parallel with `results/report.pdf` which is a report offering analyses of these results.

`models/` holds the implementations for the encoders and the NLI model

`trainer/` holds the implementation of the training regime with PyTorch Lightning

`utils/` holds functions for preparing the dataset, vocabulary, creating and loading models, and the general project config.

## Usage and dependencies

The code was written in Python 3.9.12.

### Training and using the models

To train and use the models you will need a virtual environment (not tested with `conda`):
```
# Install venv
python -m venv .venv
# Connect to it
source .venv/bin/activate
```

and install the following packages:

```
pip install torch lightning datasets torchtext torcheval
```

For example, to train a unidirectional LSTM and a bidirectional LSTM with an output size of 128 and 256 respectively (BiLSTM concatenates forward and backward passes), using a hidden size of 1024 for the NLI model and a seed 42:

```
python train.py --model unilstm bilstm --enc-hid-size 128 --nli-hid-size 1024 --seed 42
```

For more details about the training options, run
```
python train.py --help
```

### Interactively viewing logs

To interactively view the trained models' loss, accuracy, and learning rate you will need:
```
pip install tensorboard
```

After downloading and extracting the files in the root level as mentioned in the intro, run
```
tensorboard --logdir=tb_logs/
```

Note that the `average` model was run for only one epoch, so its results on tensorboard are limited.

### Evaluating representations with SentEval

To evaluate the representations using [SentEval](https://github.com/facebookresearch/SentEval), you need install SentEval first. Additional dependency for this task is
```
scikit-learn>=0.18.0
```

Clone repo:

```
git clone https://github.com/facebookresearch/SentEval.git
cd SentEval/
```

Install SentEval:
```
python setup.py install
```

Download datasets to evaluate on
```
cd data/downstream/
./get_transfer_data.bash
```

If not done already by trying to train the model, download pretrained GloVe embeddings:
```
mkdir pretrained
cd pretrained
wget http://nlp.stanford.edu/data/glove.840B.300d.zip
```

Before running, please modify lines 17-22 in `senteval.py` with your paths.

**Note! SentEval has some bugs which can be resolved following [this](https://github.com/facebookresearch/SentEval/issues/89) and [this](https://github.com/facebookresearch/SentEval/issues/94).**

Now, to evaluate the trained encoders, run
```
python senteval.py --models average unilstm bilstm bilstm_max
```

which will create pickles with dictionaries of SentEval results under `results/`.



### Development

I used the following libraries (and their official extensions for VS Code) for developing this repo which is highly advised for anyone continuing work on this repo:

```
pip install black[jupyter] flake8 isort
```

## Future TODOs

- Fix the learning rate bug in `trainer/trainer.py`, there is a TODO comment. More details are provided in `results/report.pdf` in the Analysis section.
- Add another linear layer (hid_size, hid_size) in `InferSent` under `models/nli.py`? Seems the [original implementation does this](https://github.com/ihsgnef/InferSent-1/blob/master/encoder/models.py##L818).
- Clean up `pipeline.ipynb` to be more informative of the all the steps taken to train. Also consider removing.
